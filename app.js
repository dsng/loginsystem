var express = require("express");
var path = require("path");

var app = express();

app.set("views", path.join(__dirname, "views"));
//app.engine("html", require("pug").__express);
app.set("view engine", "pug");

app.get("/", function(req, res) {
	console.log("This is the Login Page");
	//res.send("Hello World!");
	res.render("login");
});

app.listen(3000, function() {
	console.log("Listening on port 3000!")
});